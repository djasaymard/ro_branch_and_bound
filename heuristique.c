#include "heuristique.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void triInsertion(xi_t *tab, int n){
	double cle =0;
	int i =0;
	int j;
	for(j=1; j<n; j++){
		cle=tab[j].val;
		i = j-1;
		while(i>=0 && tab[i].val<cle){
			double temp = tab[i+1].val;
			tab[i+1].val = tab[i].val;
			tab[i].val = temp;
			i--;
		}
		tab[i+1].val = cle;
	}
}

int * qualiteprix(prob_t probleme){
	xi_t tabrapport[probleme.nVar];
	int * res;
	res = (int*) malloc(sizeof(int)* (probleme.nVar +1));
	int rest = probleme.valCont[0];
	
	for(int i = 0; i < probleme.nVar; i++){
		tabrapport[i].val = probleme.fonc[i]/probleme.cont[0][i];
		tabrapport[i].indice = i;
		tabrapport[i].mult = 0;
		res[i] = 0;
	}
	res[probleme.nVar] = 0;
	triInsertion(tabrapport, probleme.nVar);
	for(int i = 0; i < probleme.nVar; i++){
		tabrapport[i].mult = rest/probleme.cont[0][tabrapport[i].indice];
		rest = rest - (tabrapport[i].mult*probleme.cont[0][tabrapport[i].indice]);
		res[i] = tabrapport[i].mult;
		res[probleme.nVar] += tabrapport[i].mult * probleme.fonc[tabrapport[i].indice];
		printf("x%d = %d ", tabrapport[i].indice+1, tabrapport[i].mult);
	}
	printf("res = %d \n", res[probleme.nVar]);
	return res;

}


glp_prob * glpk(prob_t probleme, int * tabcont){
	glp_prob *prob1;
	int ia[1+probleme.nCont*probleme.nVar];									
	int ja[1+probleme.nVar*probleme.nCont];									
	double ar[1+probleme.nCont*probleme.nVar];								
	double z, x;

	prob1 = glp_create_prob();						
	glp_set_prob_name(prob1, "sacados1");			

	glp_set_obj_dir(prob1, GLP_MAX);				
	glp_add_rows(prob1, probleme.nCont);			
	for(int i = 0; i< probleme.nCont; i++){
		char temp[12];
		sprintf(temp, "machine%d", i+1);
		glp_set_row_name(prob1, i+1, temp);			
		glp_set_row_bnds(prob1, i+1, GLP_UP, 0.0, probleme.valCont[i]);
	}
	glp_add_cols(prob1, probleme.nVar);							
	for(int i=0; i< probleme.nVar; i++){
		char temp[10];
		sprintf(temp, "x%d", i+1);
		glp_set_col_name(prob1, i+1, temp);	

		if(tabcont[i]==-1)
			glp_set_col_bnds(prob1, i+1, GLP_LO, 0.0, 0.0);
	    else
			glp_set_col_bnds(prob1, i+1, GLP_FX, tabcont[i], 0.0);



		glp_set_obj_coef(prob1, i+1, probleme.fonc[i]);	
	}
	for(int i = 0; i< probleme.nCont; i++){
		for(int j = 0; j< probleme.nVar; j++){
			ia[2*i+j+1] = i+1, ja[2*i+j+1] = j+1, ar[2*i+j+1] = probleme.cont[i][j]; 
		}
	}
	
	glp_load_matrix(prob1, probleme.nCont*probleme.nVar, ia, ja, ar);
																							
	glp_simplex(prob1, NULL);
												
	z = glp_get_obj_val(prob1);	
	printf("z = %g  \n", z);					
	for(int i = 0; i< probleme.nVar; i++){
		x = glp_get_col_prim(prob1, i+1);
		printf("x%d = %g \n",i+1, x);
	}
	return prob1;						
}

int solent(glp_prob* prob, prob_t probleme){
	for(int i=0; i< probleme.nVar; i++){
		double x = glp_get_col_prim(prob, i+1),intpart, fractpart;
		fractpart = modf(x, &intpart);
		printf("x %d = %f\n",i, x);
		if(fractpart != 0.0){
			printf("solution non entiere\n");
			return 1;
		}
	}
	printf("solution entiere\n");
	return 0;
}
int max(glp_prob *prob,prob_t probleme, int indice){
	int rest = probleme.valCont[0];
	printf("\n ----- indice = %d ---- \n", indice);
	printf("\n ----rest avant = %d----\n",rest);
	int maxi = 0;
	for(int i=0; i< indice; i++){
		int x = (int) glp_get_col_prim(prob, i+1);
		
		rest = rest - (x*probleme.cont[0][i]);
		printf("\n ----rest pdt = %d----\n",rest);
	}
	maxi = rest / probleme.cont[0][indice];
	rest = rest - maxi * probleme.cont[0][indice];
	printf("\n ----rest apres = %d----\n",rest);
	
	printf("\n ----maxi = %d ---- \n", maxi);
	return maxi;
}


int * branchandbound(prob_t probleme)
{
    int * restemp;
    int borneinf;
    restemp = qualiteprix(probleme);
    borneinf = restemp[probleme.nVar];

    printf("borne inferieur  trouvee\n");
    printf("borneinf = %d\n", borneinf);
	printf("borne sup trouvee");
	noeud_t * racine;
	racine = (noeud_t*) malloc(sizeof(noeud_t));
	racine->probleme = probleme;
	racine->tabcont = (int*) malloc(sizeof(int)*probleme.nVar);
	glp_prob* solutiontemp = NULL;

	int * solutionmax = (int*) malloc(sizeof(int)*probleme.nVar);
	
	for(int i = 0; i < probleme.nVar; i++){
		racine->tabcont[i] = -1;
		solutionmax[i] = 0;
		printf("restemp %d = %d \n", i, restemp[i]);
	}
	printf("borneinf = %d \n", borneinf);
	glp_prob * bornesup = glpk(probleme, racine->tabcont);
	racine->glp = bornesup;

	racine->indice = 0;

	
	arbre_t * arbre;
	arbre = (arbre_t*) malloc(sizeof(arbre));
	arbre->racine = racine;

	pile_t * pile;
	pile = (pile_t*) malloc(sizeof(pile));
	pile->noeud = (noeud_t**) malloc(sizeof(noeud_t *) * 50);
	pile->sommet = 0;
	for(int i =0; i<50; i++){
		pile->noeud[i] = (noeud_t*) malloc(sizeof(noeud_t));
		
	}
	empiler(pile,arbre->racine);
	int k = 0;
	while(pile_vide(pile) == 0){
		printf("\n\n\n -------- iteration %d ------ \n\n\n", k);
		noeud_t * noeudtemp = depiler(pile);
		int znoeud = glp_get_obj_val(noeudtemp->glp);
		if(znoeud > borneinf){
			if(solent(noeudtemp->glp, probleme) == 1){
				int poss = max(noeudtemp->glp,probleme, noeudtemp->indice) +1;
				noeudtemp->noeud = (noeud_t**) malloc(sizeof(noeud_t*) * poss);
				
				for(int i = 0; i < poss; i++){
					noeudtemp->noeud[i] = (noeud_t*) malloc(sizeof(noeud_t));
					//noeudtemp->noeud[i]->tabcont = noeudtemp->tabcont;
					noeudtemp->noeud[i]->tabcont = (int*) malloc(sizeof(int)*probleme.nVar);
					for(int j = 0; j < probleme.nVar; j++)
						noeudtemp->noeud[i]->tabcont[j] = noeudtemp->tabcont[j];
					noeudtemp->noeud[i]->tabcont[noeudtemp->indice] = i;
					printf("\n\n------- tabcont\n");
					for(int j = 0; j < probleme.nVar; j++){
						printf("indice %d = %d\n", j,noeudtemp->noeud[i]->tabcont[j]);
					}
					printf("-----\n");
					printf("tabcont %d = %d \n", 0, racine->tabcont[0]);
					noeudtemp->noeud[i]->indice = noeudtemp->indice + 1;
					noeudtemp->noeud[i]->glp = glpk(probleme, noeudtemp->noeud[i]->tabcont);

					empiler(pile,noeudtemp->noeud[i]);
				}
			}
			else{
				borneinf = glp_get_obj_val(noeudtemp->glp);
				printf("yolo\n");
			    solutiontemp = noeudtemp->glp; // borneinf->tabcont;
			}
		}
		k++;
		printf("\n----- fin iteration ---\n");
		
	} 
	if(solutiontemp == NULL)
		return restemp;
	for(int i = 0; i < probleme.nVar; i++){
		solutionmax[i] = (int)glp_get_col_prim(solutiontemp, i+1);
	}
	return solutionmax; //tabcont;


}
