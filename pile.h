#include "types.h"

typedef struct pile_type{
	noeud_t ** noeud;
	int sommet;
}pile_t;

int pile_vide(pile_t *);
void empiler(pile_t *,noeud_t*);
noeud_t * depiler(pile_t *);