CC = gcc -Wall
OBJETS = simplexe.o probleme.o heuristique.o pile.o

simplexe : $(OBJETS)
	$(CC) -o simplexe $(OBJETS) -lglpk

simplexe.o: simplexe.c simplexe.h probleme.h types.h
	$(CC) -c simplexe.c
	
probleme.o: probleme.c probleme.h types.h
	$(CC) -c probleme.c

heuristique.o: heuristique.c heuristique.h types.h
	$(CC) -c heuristique.c

pile.o:pile.c pile.h 
	$(CC) -c pile.c 

clean:
	rm simplexe $(OBJETS) *
