#ifndef _TYPES_H
#define _TYPES_H

#include <glpk.h>	

typedef struct prob_type {
	int nVar;			/*nombre de variables*/
	int nCont;			/*nombre de contraintes*/
	int typeOpt;		/*type d'optimisation : 0 si min et 1 si max*/
	double *fonc;		/*fonction objectif, coefficients des xj*/
	double **cont;		/*contraintes, coefficients des aij*/
	int *signeCont;		/*signe de chaque contrainte, 0 si <=, 1 si >= */
	double *valCont;	/*valeur associee a chaque contrainte : bi*/
} prob_t;

typedef struct xi_type{
	double val;
	int indice;
	int mult;
} xi_t;

typedef struct noeud_type
{
	prob_t probleme;
	glp_prob * glp;
	int * tabcont;
	int indice;
	struct noeud_type ** noeud;
}noeud_t;

typedef struct arbre_type
{
   noeud_t * racine;
}arbre_t;


#endif
